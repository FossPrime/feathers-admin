# WIP

# Feathers Admin

A feathers based mongo-express, phpMyAdmin, MongoDB Compass alternative.

Requires a server side service to work. Modular enough to be dropped in to feathers client apps that support web components.